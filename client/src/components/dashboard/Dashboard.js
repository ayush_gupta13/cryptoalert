import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  renderList() {
    var requestOptions = {
      method: 'GET',
      mode: 'cors',
      redirect: 'follow',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
    };

    fetch("https://api.coincap.io/v2/assets/bitcoin/history?interval=d1", requestOptions)
    .then((res) => res.json())
    .then((result) => {
      this.setState({
        data : result ? result.data : [],
      })
    })
    .catch((err) => {
      console.log(err);
    })

    console.log(this.state.data);

    let dataList = []
    this.state.data.map(item => {
        return dataList.push(`<li key={item.id}>{item.name}</li>`)
    })
    return dataList;   
  }

  render() {
    const { user } = this.props.auth;

    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="landing-copy col s12 center-align">
            <h4>
              <p className="flow-text grey-text text-darken-1">
                <button
                  style={{
                    width: "150px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                    marginTop: "1rem"
                  }}
                  onClick={this.renderList}
                  className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                >
                  Get
                </button>
                <span style={{ fontFamily: "monospace" }}></span> 
              </p>
            </h4>
            <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);
