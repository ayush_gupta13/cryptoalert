**This project uses the following technologies**

[React] for frontend
[Express] and [Nodejs] for the backend
[MongoDB]for the database
[Redux] for state management between React components

**Quick Start**

Install dependencies for server & client :-
npm install 

Run client & server with concurrently :-
node server.js (server)
npm start (client)

Server runs on http://localhost:5000 and client on http://localhost:3000

